﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppPropina
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            this.btnCalcular.Clicked += BtnCalcular_Clicked;
		}

        private void BtnCalcular_Clicked(object sender, EventArgs e)
        {
            var total= decimal.Parse(this.entradaTotal.Text);
            var propina = decimal.Parse(this.entradaPropina.Text) / 100;
            var numPersonas = decimal.Parse(this.entradaNumPersonas.Text);

            var totalPropina = total * propina;
            var totalC = total + totalPropina;

            this.txtPropina.Detail = (totalPropina).ToString("C");
            this.txtTotal.Detail = (totalC).ToString("C");
            this.txtPropinaPersona.Detail = (totalPropina / numPersonas).ToString("C");
            this.txtTotalPersona.Detail = (totalC / numPersonas).ToString("C");
        }
    }
}
